# How To
Just run the cells in ML_agoge.ipynb notebook (might be heavy for a local machine)

# ML-Agoge Context
Explain:

Preprocess text
Process labels (one-hot vector with len(cols) = n_labels)
Train / test split simple explanation 60/40
Words are represented by input_ids
Text / word tokens into embeddings

Dimensionality is 512 tokens
Word embeddings (king-man+woman = queen)


Show encode/decode flow
Include text and image flows
Metrics explanation

https://jalammar.github.io/explaining-transformers/